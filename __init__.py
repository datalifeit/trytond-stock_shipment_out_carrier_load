# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import carrier


def register():
    Pool.register(
        carrier.AssociateCarrierLoad,
        carrier.PrintCarrierNote,
        module='stock_shipment_out_carrier_load', type_='wizard')
