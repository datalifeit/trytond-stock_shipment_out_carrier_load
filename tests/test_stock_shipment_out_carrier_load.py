# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.tests.test_tryton import ModuleTestCase


class StockShipmentOutCarrierLoadTestCase(ModuleTestCase):
    """Test Stock Shipment Out Carrier Load module"""
    module = 'stock_shipment_out_carrier_load'


del ModuleTestCase
